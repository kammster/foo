# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Foo::Application.config.secret_key_base = 'db57bb7f62307ce7ed0cd389af4473e1b7e3d8353b4a8be99c38517d0ba16d2a6b41958d458220f54a4258ce447c86b469bc11aa7f3201e3ebd377f3679d93a5'
